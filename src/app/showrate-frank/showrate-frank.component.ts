import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-showrate-frank',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './showrate-frank.component.html',
  styleUrl: './showrate-frank.component.css'
})
export class ShowrateFrankComponent implements OnInit {
  constructor( private http : HttpClient) { }

  city : any ='';
  data : any ;
  tab_rates : any[] = [];
  curse_date : any[] = [];
  curse_value : any [] = [];
  tab : number[] = [];

  ngOnInit() {
   this.fetchData();
  }
   
  private fetchData(){
    this.http.get('http://api.nbp.pl/api/exchangerates/rates/a/chf/last/10/?format=json').subscribe((res)=>{
      this.data = res; 

      for ( let d of this.data['rates']){
        this.tab_rates.push(d['effectiveDate']) ;
        this.tab_rates.push(d['mid']) ;  
      }
      for(let i = 0; i < this.tab_rates.length; i+=2){
        this.curse_date.push(this.tab_rates[i]);
      }
      
      for(let i = 1; i < this.tab_rates.length; i+=2){
        this.curse_value.push(this.tab_rates[i]);
      } 
    });
  }

  
}
