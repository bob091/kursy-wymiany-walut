import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowrateFrankComponent } from './showrate-frank.component';

describe('ShowrateFrankComponent', () => {
  let component: ShowrateFrankComponent;
  let fixture: ComponentFixture<ShowrateFrankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShowrateFrankComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShowrateFrankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
