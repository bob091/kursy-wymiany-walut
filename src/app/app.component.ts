import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';

import { HttpClientModule } from '@angular/common/http'; //Klient Http
import { ShowExchengeRateComponent } from './show-exchenge-rate/show-exchenge-rate.component';
import { ShowrateFrankComponent } from './showrate-frank/showrate-frank.component';
import { ShowratePoundComponent } from './showrate-pound/showrate-pound.component';
import { ShowIndexComponent } from './show-index/show-index.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, 
            RouterOutlet, 
            ShowExchengeRateComponent, 
            ShowrateFrankComponent, 
            ShowratePoundComponent,
            ShowIndexComponent, 
            HttpClientModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'kurs-wymiany-walut';
}
