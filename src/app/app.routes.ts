import { Routes, RouterModule } from '@angular/router';
import { ShowExchengeRateComponent } from './show-exchenge-rate/show-exchenge-rate.component';
import { ShowratePoundComponent } from './showrate-pound/showrate-pound.component';
import { ShowrateFrankComponent } from './showrate-frank/showrate-frank.component';
import { ShowIndexComponent } from './show-index/show-index.component';

export const routes: Routes = [
    {path : '', component : ShowIndexComponent},
    {path : 'dolar', component : ShowExchengeRateComponent},
    {path : 'frank', component : ShowrateFrankComponent},
    {path : 'pound', component : ShowratePoundComponent},
];
