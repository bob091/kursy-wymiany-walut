import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowExchengeRateComponent } from './show-exchenge-rate.component';

describe('ShowExchengeRateComponent', () => {
  let component: ShowExchengeRateComponent;
  let fixture: ComponentFixture<ShowExchengeRateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShowExchengeRateComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShowExchengeRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
