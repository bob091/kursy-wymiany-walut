import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

import Chart from 'chart.js/auto';


import { HttpClient, HttpHeaders  } from '@angular/common/http'; //Klient Http
import { OnInit }     from '@angular/core';
/** doloar emerykański */
@Component({
  selector: 'app-show-exchenge-rate-dolar',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './show-exchenge-rate.component.html',
  styleUrl: './show-exchenge-rate.component.css'
})
export class ShowExchengeRateComponent implements OnInit {
  constructor( private http : HttpClient) { }

  city : any ='';
  data : any ;
  tab_rates : any[] = [];
  curse_date : any[] = [];
  curse_value : any [] = [];

  public chart: any;

  ngOnInit() {
    this.fetchData();
    this.createChart(this.curse_date);
  }
   
  private fetchData(){
    this.http.get('http://api.nbp.pl/api/exchangerates/rates/a/usd/last/10/?format=json').subscribe((res)=>{
      this.data = res; 

      for ( let d of this.data['rates']){
        this.tab_rates.push(d['effectiveDate']) ;
        this.tab_rates.push(d['mid']) ;  
      }

      for(let i = 0; i < this.tab_rates.length; i+=2){
        this.curse_date.push(this.tab_rates[i]);
      }
      for(let i = 1; i < this.tab_rates.length; i+=2){
        this.curse_value.push(this.tab_rates[i]);
      } 
    });
  }

  
  private createChart(curse_date :any){

    console.log(curse_date);
    
    this.chart = new Chart("MyChart", {
      type: 'bar', //this denotes tha type of chart
        
      
      data: {// values on X-Axis
        labels: ['2022-05-10', '2022-05-11', '2022-05-12','2022-05-13',
        '2022-05-14', '2022-05-15', '2022-05-16','2022-05-17', '2022-05-16','2022-05-17'], 
	       datasets: [
          {
            label: "Profit",
            data: ['542', '542', '536', '327', '17',
									 '0.00', '538', '541','00', '00',],
            backgroundColor: 'limegreen'
          }  
        ]
      },
      options: {
        aspectRatio:2.5
      }
      
    });
  }

}
