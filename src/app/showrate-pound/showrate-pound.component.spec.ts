import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowratePoundComponent } from './showrate-pound.component';

describe('ShowratePoundComponent', () => {
  let component: ShowratePoundComponent;
  let fixture: ComponentFixture<ShowratePoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ShowratePoundComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ShowratePoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
