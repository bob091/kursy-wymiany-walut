import { Component, OnInit, Input} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-showrate-pound',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './showrate-pound.component.html',
  styleUrl: './showrate-pound.component.css'
})
export class ShowratePoundComponent implements OnInit {
  constructor( private http : HttpClient) { }

  city : any ='';
  data : any ;
  tab_rates : any[] = [];
  curse_date : any[] = [];
  curse_value : any [] = [];
  //tab : number[] = [];
 // @Input() ithem = "ala ";

  ngOnInit() {
    this.fetchData();
  }
   
  private fetchData(){

    this.http.get('http://api.nbp.pl/api/exchangerates/rates/a/gbp/last/10/?format=json').subscribe((res)=>{
      this.data = res; 

      for ( let d of this.data['rates']){
        this.tab_rates.push(d['effectiveDate']) ;
        this.tab_rates.push(d['mid']) ;  
      }
      for(let i = 0; i < this.tab_rates.length; i+=2){
        this.curse_date.push(this.tab_rates[i]);
      }
      
      for(let i = 1; i < this.tab_rates.length; i+=2){
        this.curse_value.push(this.tab_rates[i]);
      } 
    });
  }
}
